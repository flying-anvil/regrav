﻿using UnityEngine;

namespace FlyingAnvil.ReGrav.ReGrav3D
{
    [RequireComponent(typeof(GravityRedirector))]
    public class GravityOrientation : MonoBehaviour
    {
        [SerializeField, Range(0f, 10f)]
        private float strength = 5f;

        private GravityRedirector reGrav;
        private Rigidbody rb;

        private void Start()
        {
            reGrav = GetComponent<GravityRedirector>();
            rb = GetComponent<Rigidbody>();
            rb.freezeRotation = true;
        }

        private void Update()
        {
            // TODO: Fix snapping

            transform.rotation = Quaternion.LookRotation(-reGrav.GetDirection(), -transform.forward);
//            transform.rotation = Quaternion.Lerp(
//                transform.rotation,
//                Quaternion.LookRotation(-reGrav.GetDirection(), -transform.forward),
//                Time.deltaTime * strength
//            );
            transform.Rotate(Vector3.right, 90f);

//            transform.up = Vector3.Lerp(transform.up, -reGrav.GetDirection(), Time.deltaTime * strength);
        }
    }
}
