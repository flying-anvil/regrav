using System.Linq;
using UnityEngine;

namespace FlyingAnvil.ReGrav.ReGrav3D
{
    public class CollisionToGravity : MonoBehaviour
    {
        [SerializeField]
        private GravityRedirector reGrav;

        [SerializeField]
        private LayerMask groundLayer;

        // ReSharper disable once Unity.RedundantEventFunction
        private void Start()
        {
            // To make the Component de-avtiatable
        }

        private void OnCollisionStay(Collision other)
        {
            if (!enabled)
            {
                return;
            }

            if ((groundLayer.value & 1 << other.gameObject.layer) != 0)
            {
                Vector3 allTheDirections = other.contacts.Aggregate(
                    Vector3.zero,
                    (current, contact) => current - contact.normal
                );

                reGrav.SetDirection(allTheDirections / other.contacts.Length);
            }
        }
    }
}
