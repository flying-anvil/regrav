using UnityEngine;
using UnityEngine.Serialization;

namespace FlyingAnvil.ReGrav.ReGrav3D
{
    [RequireComponent(typeof(Rigidbody))]
    public class SimpleMovement : MonoBehaviour
    {
        [SerializeField]
        private float movementSpeed = 5f;

        [SerializeField]
        private float rotationSpeed = 5f;

        private Rigidbody rb;

        private void Start()
        {
            rb = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            Vector3 movement = Vector3.zero;
            float rotation = 0f;

            if (Input.GetKey(KeyCode.W))
            {
                movement += transform.forward;
            }

            if (Input.GetKey(KeyCode.S))
            {
                movement -= transform.forward;
            }

            if (Input.GetKey(KeyCode.A))
            {
                rotation -= Time.deltaTime;
            }

            if (Input.GetKey(KeyCode.D))
            {
                rotation += Time.deltaTime;
            }

            rb.position += movementSpeed * Time.deltaTime * movement;
            transform.Rotate(transform.up, rotation * rotationSpeed, Space.World);
        }
    }
}
