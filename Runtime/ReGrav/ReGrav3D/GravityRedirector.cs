﻿using UnityEngine;

namespace FlyingAnvil.ReGrav.ReGrav3D
{
    [RequireComponent(typeof(Rigidbody))]
    public class GravityRedirector : MonoBehaviour
    {
        [SerializeField] private Vector3 direction = Physics.gravity.normalized;

        [SerializeField] private bool normalize = true;

        [SerializeField] private float multiplier = Physics.gravity.magnitude;

        private Rigidbody rb;

        private void Start()
        {
            rb = GetComponent<Rigidbody>();

//            if (!rb.useGravity)
//            {
//                enabled = false;
//                return;
//            }
//
//            rb.useGravity = false;
        }

        private void FixedUpdate()
        {
            Vector3 force = normalize
                ? direction.normalized
                : direction;

            rb.AddForce(rb.mass * multiplier * force);
        }

        public void SetDirection(Vector3 newDirection)
        {
            direction = newDirection;
        }

        public Vector3 GetDirection()
        {
            return direction;
        }

        public void SetMultiplier(float newMultiplier)
        {
            multiplier = newMultiplier;
        }

        public float GetMultiplier()
        {
            return multiplier;
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            Rigidbody rigidBody = GetComponent<Rigidbody>();

            if (!rigidBody)
            {
                return;
            }

            Gizmos.color = Color.green;
            Vector3 position = rigidBody.position;

            Vector3 targetDirection = normalize
                ? direction.normalized
                : direction;

            Gizmos.DrawLine(position, position + (targetDirection * multiplier));
        }
#endif
    }
}
