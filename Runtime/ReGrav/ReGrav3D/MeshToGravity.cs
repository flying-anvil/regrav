using UnityEngine;

namespace FlyingAnvil.ReGrav.ReGrav3D
{
    [RequireComponent(typeof(GravityRedirector))]
    public class MeshToGravity : MonoBehaviour
    {
        [SerializeField]
        private float maxDistanceToGround = 100f;

        [SerializeField]
        private LayerMask groundLayer;

        private GravityRedirector reGrav;

        private void Start()
        {
            reGrav = GetComponent<GravityRedirector>();
        }

        private void Update()
        {
            Ray ray = new Ray(transform.position, -transform.up);

            // TODO: Remove debug
            Debug.DrawRay(transform.position, ray.direction * maxDistanceToGround, Color.red);
            
            if (Physics.Raycast(ray, out RaycastHit hit, maxDistanceToGround, groundLayer))
            {
                // TODO: Remove debug
                Debug.DrawRay(hit.point, hit.normal.normalized, Color.cyan);
                reGrav.SetDirection(-hit.normal.normalized);
            }
        }
    }
}
