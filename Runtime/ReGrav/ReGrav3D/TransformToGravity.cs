using UnityEngine;

namespace FlyingAnvil.ReGrav.ReGrav3D
{
    [RequireComponent(typeof(GravityRedirector))]
    public class TransformToGravity : MonoBehaviour
    {
        public Transform center;
    
        private GravityRedirector reGrav;
        private Rigidbody rb;

        private void Start()
        {
            reGrav = GetComponent<GravityRedirector>();
            rb = GetComponent<Rigidbody>();
        }

        private void Update()
        {
            Vector3 direction = center.position - rb.position;
        
            reGrav.SetDirection(direction);
        }
    }
}
