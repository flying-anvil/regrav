using UnityEngine;

namespace FlyingAnvil.ReGrav.ReGrav2D
{
    [RequireComponent(typeof(GravityRedirector2D))]
    public class ColliderToGravity : MonoBehaviour
    {
        [SerializeField]
        private float maxDistanceToGround = 100f;

        [SerializeField]
        private LayerMask groundLayer;

        private GravityRedirector2D reGrav;

        private void Start()
        {
            reGrav = GetComponent<GravityRedirector2D>();
        }

        private void Update()
        {
            Vector2 position = transform.position;

            // TODO: Remove debug
//            Debug.DrawRay(position, -transform.up * maxDistanceToGround, Color.red);

            RaycastHit2D hit = Physics2D.Raycast(position, -transform.up, maxDistanceToGround, groundLayer);
            
            if (hit.collider)
            {
                // TODO: Remove debug
                Debug.DrawRay(hit.point, hit.normal.normalized, Color.cyan);
                reGrav.SetDirection(-hit.normal.normalized);
            }
        }

    }
}
