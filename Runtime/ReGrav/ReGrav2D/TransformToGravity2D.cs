using UnityEngine;

namespace FlyingAnvil.ReGrav.ReGrav2D
{
    [RequireComponent(typeof(GravityRedirector2D))]
    public class TransformToGravity2D : MonoBehaviour
    {
        public Transform center;
    
        private GravityRedirector2D reGrav;

        private void Start()
        {
            reGrav = GetComponent<GravityRedirector2D>();
        }

        private void Update()
        {
            Vector2 direction = center.position - transform.position;
        
            reGrav.SetDirection(direction);
        }
    }
}
