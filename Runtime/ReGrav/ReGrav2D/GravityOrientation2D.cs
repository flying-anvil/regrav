using UnityEngine;

namespace FlyingAnvil.ReGrav.ReGrav2D
{
    [RequireComponent(typeof(GravityRedirector2D))]
    public class GravityOrientation2D : MonoBehaviour
    {
        [SerializeField, Range(0f, 10f)]
        private float strength = 5f;

        private GravityRedirector2D reGrav;
        private Rigidbody2D rb;

        private void Start()
        {
            reGrav = GetComponent<GravityRedirector2D>();
            rb = GetComponent<Rigidbody2D>();
            rb.freezeRotation = true;
        }

        private void Update()
        {
            transform.up = Vector2.Lerp(transform.up, -reGrav.GetDirection(), Time.deltaTime * strength);
        }
    }
}
