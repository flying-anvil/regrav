using UnityEngine;

namespace FlyingAnvil.ReGrav.ReGrav2D
{
    public class SimpleMovement2D : MonoBehaviour
    {
        [SerializeField]
        private float movementSpeed = 5f;

        private Rigidbody2D rb;

        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            Vector3 movement = Vector3.zero;
            
            if (Input.GetKey(KeyCode.A))
            {
                movement -= transform.right;
            }

            if (Input.GetKey(KeyCode.D))
            {
                movement += transform.right;
            }

            rb.position += movementSpeed * Time.deltaTime * (Vector2)movement;
        }
    }
}
