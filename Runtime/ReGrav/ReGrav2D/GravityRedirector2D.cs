using UnityEngine;

namespace FlyingAnvil.ReGrav.ReGrav2D
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class GravityRedirector2D : MonoBehaviour
    {
        [SerializeField] private Vector2 direction = Physics.gravity.normalized;

        [SerializeField] private bool normalize = true;

        [SerializeField] private float multiplier = Physics.gravity.magnitude;

        private Rigidbody2D rb;

        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();

//            if (!rb.useGravity)
//            {
//                enabled = false;
//                return;
//            }
//
//            rb.useGravity = false;
        }

        private void FixedUpdate()
        {
            Vector2 force = normalize
                ? direction.normalized
                : direction;

            rb.AddForce(rb.mass * multiplier * force);
        }

        public void SetDirection(Vector2 newDirection)
        {
            direction = newDirection;
        }

        public Vector2 GetDirection()
        {
            return direction;
        }

        public void SetMultiplier(float newMultiplier)
        {
            multiplier = newMultiplier;
        }

        public float GetMultiplier()
        {
            return multiplier;
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.green;
            Vector2 position = transform.position;

            Vector2 targetDirection = normalize
                ? direction.normalized
                : direction;

            Gizmos.DrawLine(position, position + (targetDirection * multiplier));
        }
#endif
    }
}
