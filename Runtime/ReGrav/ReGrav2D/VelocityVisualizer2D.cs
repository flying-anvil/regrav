using UnityEngine;

namespace FlyingAnvil.ReGrav.ReGrav2D
{
    public class VelocityVisualizer2D : MonoBehaviour
    {
        private new Rigidbody2D rigidbody2D;

        private void Start()
        {
            rigidbody2D = GetComponent<Rigidbody2D>();
        }

        private void OnDrawGizmos()
        {
            if (!rigidbody2D)
            {
                return;
            }
            
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(rigidbody2D.position, rigidbody2D.position + rigidbody2D.velocity);
        }
    }
}
