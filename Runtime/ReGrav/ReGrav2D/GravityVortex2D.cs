using System;
using System.Collections.Generic;
using UnityEngine;

namespace ReGrav.FlyingAnvil.ReGrav.ReGrav2D
{
    public class GravityVortex2D : MonoBehaviour
    {
        [SerializeField]
        private VortexType vortexType = VortexType.Fixed;

        [SerializeField]
        private float attractionRange = 3f;

        [Tooltip("Acceleration for Fixed and Linear types, mass for Mass Based")]
        [SerializeField]
        private float vortexStrength = Physics.gravity.magnitude * 2;

        [SerializeField]
        private bool detract;

        private List<Rigidbody2D> gravitees = new List<Rigidbody2D>();
        private CircleCollider2D collider2d;
        private float lastRange;

        public enum VortexType
        {
            Fixed,
            Linear,
            MassBased,
        }

        private void Start()
        {
            collider2d = gameObject.AddComponent<CircleCollider2D>();
            collider2d.radius = attractionRange;
            collider2d.isTrigger = true;

            lastRange = attractionRange;

            // ReSharper disable once Unity.PreferNonAllocApi
//            Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, attractionRange);
//            foreach (Collider2D currentCollider in colliders)
//            {
//                Rigidbody2D currentRigidbody = currentCollider.GetComponent<Rigidbody2D>();
//
//                if (currentRigidbody)
//                {
//                    gravitees.Add(currentRigidbody);
//                }
//            }
        }

        private void Update()
        {
            if (Math.Abs(lastRange - attractionRange) > .001f)
            {
                collider2d.radius = attractionRange;
            }
        }

        private void FixedUpdate()
        {
            int detractMultiplier = detract ? -1 : 1;
            
            switch (vortexType)
            {
                case VortexType.Fixed:
                    foreach (Rigidbody2D gravitee in gravitees)
                    {
                        Vector2 direction = (Vector2)transform.position - gravitee.position;
                        
                        gravitee.AddForce(detractMultiplier * vortexStrength * gravitee.mass * direction);
                    }
                    break;
                case VortexType.Linear:
                    foreach (Rigidbody2D gravitee in gravitees)
                    {
                        Vector2 distance = (Vector2)transform.position - gravitee.position;
                        Vector2 direction = distance.normalized;
                        float proportionalStrength = Mathf.Max(1 - distance.magnitude / attractionRange, 0f);

                        gravitee.AddForce(detractMultiplier * vortexStrength * gravitee.mass * proportionalStrength * direction);
                    }
                    break;
                case VortexType.MassBased:
                    foreach (Rigidbody2D gravitee in gravitees)
                    {
                        Vector2 direction = (Vector2)transform.position - gravitee.position;
                        float sqDistance = Vector2.SqrMagnitude(direction);
                        direction.Normalize();

                        float graviteeMass = gravitee.mass;
                        float mass = vortexStrength * graviteeMass;
                        float strength = Mathf.Min(.667408f * (mass / sqDistance), 1000f);

                        gravitee.AddForce(detractMultiplier * graviteeMass * strength * direction);
                    }
                    break;
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Rigidbody2D otherRigidbody = other.GetComponent<Rigidbody2D>();
            if (otherRigidbody)
            {
                gravitees.Add(otherRigidbody);
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            Rigidbody2D otherRigidbody = other.GetComponent<Rigidbody2D>();
            if (otherRigidbody)
            {
                gravitees.Remove(otherRigidbody);
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color32(48, 32, 75, 255);

            Gizmos.DrawWireSphere(transform.position, attractionRange);

            Gizmos.color = Color.green;

            foreach (Rigidbody2D gravitee in gravitees)
            {
                Gizmos.DrawLine(transform.position, gravitee.position);
            }
        }
    }
}
